﻿using UnityEngine;
using System.Collections;

public class LiquidHandler : MonoBehaviour
{
    public Material _ParticleMat;
    public ParticleSystem _Particle;
    public Material _BottleMat;
    public Color _LiqColor;

    private Color mLiqColor;
    public Color LiqColor
    {
        get
        {
            return mLiqColor;
        }
        set
        {
            mLiqColor = value;
            SetColour(mLiqColor);
        }
    }

    void Start()
    {
        LiqColor = _LiqColor;
    }

    public void SetColour(Color col)
    {
        _ParticleMat.SetColor("_TintColor", col);
        _BottleMat.color = col;
    }
    public void ResetParticle()
    {
        _Particle.Stop();
        _Particle.Clear();
    }

    public void StartParticle()
    {
        _Particle.Play();
    }

    public void OnColorSelected(Color clr)
    {
        LiqColor = clr;
    }
}
