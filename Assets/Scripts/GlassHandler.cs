﻿using UnityEngine;
using System.Collections;

public class GlassHandler : MonoBehaviour {

    public GameObject _LiquidObject;
    public float _MaxSize;
    public float _LiquidFillfactor;
    public float _StartDelay;
    public Material _LiquidMat;
    
    public LiquidHandler _Colour1;
    public LiquidHandler _Colour2;

    public ColorPicker _Picker;
    public GameObject _ChangeButtons;   
    private float mSize;
    private Color _ActiveColour, _PreviousColour;
    private bool mIsEditing;
	void Start ()
    {
        Reset();
	}

	void Update ()
    {
        //_ActiveColour = _Colour1._BottleMat.color + _Colour2._BottleMat.color;
        //_LiquidMat.color = _ActiveColour;
        //if (!_PreviousColour.Equals(_ActiveColour) && !mIsEditing)
        //{
        //    mIsEditing = true;
        //    Reset();
        //}

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    mIsEditing = true;
        //    Reset();
        //}
	}

    public void Stop()
    {
        StopAllCoroutines();
        _LiquidObject.transform.localScale = new Vector3(1, 0.1f, 1);
        mSize = 0;
        _Colour1.ResetParticle();
        _Colour2.ResetParticle();
        mIsEditing = true;
    }

    public void Reset()
    {
        _Colour1.StartParticle();
        _Colour2.StartParticle();
        _ActiveColour = _Colour1._BottleMat.color + _Colour2._BottleMat.color;
        StartCoroutine("FillWater");
        StartCoroutine("ChangeColor",_ActiveColour);
    }

    IEnumerator FillWater()
    {
        yield return new WaitForSeconds(_StartDelay);
        while(mSize < _MaxSize)
        {
            mSize += _LiquidFillfactor;
            _LiquidObject.transform.localScale += new Vector3(0, _LiquidFillfactor, 0);
            yield return new WaitForSeconds(_LiquidFillfactor);
        }
    }

    IEnumerator ChangeColor(Color inputColor)
    {
        yield return new WaitForSeconds(_StartDelay);
        while (!_PreviousColour.Equals(inputColor))
        {
            _LiquidMat.color = Color.Lerp(_LiquidMat.color, inputColor, _LiquidFillfactor);
            yield return new WaitForSeconds(_LiquidFillfactor);
        }
        _PreviousColour = _ActiveColour;
        mIsEditing = false;
    }

    public void OnClr1Update()
    {
        Stop();
        _ChangeButtons.SetActive(false);
        _Picker.gameObject.SetActive(true);
        _Picker.onValueChanged.AddListener(_Colour1.OnColorSelected);       
    }

    public void OnClr2Update()
    {
        Stop();
        _ChangeButtons.SetActive(false);
        _Picker.gameObject.SetActive(true);
        _Picker.onValueChanged.AddListener(_Colour2.OnColorSelected);
    }

    public void ClosePicker()
    {
        Reset();
        _ChangeButtons.SetActive(true);
        _Picker.onValueChanged.RemoveAllListeners();
        _Picker.gameObject.SetActive(false);
    }

}
